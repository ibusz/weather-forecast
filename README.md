# Weather Forecast

## How to build/Test application
**Step:**
1. Open terminal and go to directory
2. ```pod install``` (To download library)
2. Open the workspace using Xcode (weather.xcworkspace)
3. Select Device / Simulator
4. Run

## Requirements
- iOS 9.0+
- Xcode 9
- Swift4
