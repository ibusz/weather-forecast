//
//  Created by Pongprera Punnyarphattreephop on 8/8/18.
//  Copyright © 2018 Pongprera. All rights reserved.
//

import XCTest
import SwiftyJSON
@testable import weather

class WeatherTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testWeatherModel() {
        let response = "{\"coord\":{\"lon\":-0.13,\"lat\":51.51},\"weather\":[{\"id\":520,\"main\":\"Rain\",\"description\":\"light intensity shower rain\",\"icon\":\"09d\"}],\"base\":\"stations\",\"main\":{\"temp\":295.14,\"pressure\":1011,\"humidity\":36,\"temp_min\":293.15,\"temp_max\":297.15},\"visibility\":10000,\"wind\":{\"speed\":6.7,\"deg\":240},\"clouds\":{\"all\":0},\"dt\":1533745200,\"sys\":{\"type\":1,\"id\":5091,\"message\":0.0059,\"country\":\"GB\",\"sunrise\":1533702950,\"sunset\":1533756915},\"id\":2643743,\"name\":\"London\",\"cod\":200}"
        
        let weather = Weather(json: JSON(parseJSON: response))
        
        XCTAssertEqual(String(format: "%.2f", (weather?.coord.latitude)!), "51.51")
        XCTAssertEqual(String(format: "%.2f", (weather?.coord.longitude)!), "-0.13")
        XCTAssertEqual(weather?.weatherInfo[0].id, 520)
        XCTAssertEqual(weather?.weatherInfo[0].main, "Rain")
        XCTAssertEqual(weather?.weatherInfo[0].description, "light intensity shower rain")
        XCTAssertEqual(weather?.weatherInfo[0].icon, "09d")
        XCTAssertEqual(weather?.base, "stations")
        XCTAssertEqual(String(format: "%.2f", (weather?.main.temp.celsius)!), "21.99")
        XCTAssertEqual(weather?.main.pressure, 1011)
        XCTAssertEqual(weather?.main.humidity, 36)
        XCTAssertEqual(String(format: "%.2f", (weather?.main.temp_min.celsius)!), "20.00")
        XCTAssertEqual(String(format: "%.2f", (weather?.main.temp_max.celsius)!), "24.00")
        XCTAssertEqual(weather?.visibility, 10000)
        XCTAssertEqual(weather?.wind.speed, 6.7)
        XCTAssertEqual(weather?.wind.deg, 240)
        XCTAssertEqual(weather?.clouds.all, 0)
        XCTAssertEqual(weather?.dt, Date(millisec: 1533745200)!)
        XCTAssertEqual(weather?.sys.type, 1)
        XCTAssertEqual(weather?.sys.id, 5091)
        XCTAssertEqual(weather?.sys.message, 0.0059)
        XCTAssertEqual(weather?.sys.country, "GB")
        XCTAssertEqual(weather?.sys.sunrise, Date(millisec: 1533702950)!)
        XCTAssertEqual(weather?.sys.sunset, Date(millisec: 1533756915)!)
        XCTAssertEqual(weather?.id, 2643743)
        XCTAssertEqual(weather?.name, "London")
        XCTAssertEqual(weather?.cod, 200)
    }
    
}
