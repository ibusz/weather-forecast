//
//  Created by Pongprera Punnyarphattreephop on 8/8/18.
//  Copyright © 2018 Pongprera. All rights reserved.
//

import XCTest
@testable import weather

class TemperatureUtilTest: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testKelvinToCelsius() {
        XCTAssertEqual(String(format: "%.2f", TemperatureUtils.kelvinToCelsius(with: 0)), "-273.15")
        XCTAssertEqual(String(format: "%.2f", TemperatureUtils.kelvinToCelsius(with: 273.15)), "0.00")
        XCTAssertEqual(String(format: "%.2f", TemperatureUtils.kelvinToCelsius(with: 1000)), "726.85")
    }
    
    func testKelvinToFahrenheit() {
        XCTAssertEqual(String(format: "%.2f", TemperatureUtils.kelvinToFahrenheit(with: 0)), "-459.67")
        XCTAssertEqual(String(format: "%.2f", TemperatureUtils.kelvinToFahrenheit(with: 260)), "8.33")
        XCTAssertEqual(String(format: "%.2f", TemperatureUtils.kelvinToFahrenheit(with: 1000)), "1340.33")
    }
    
    func testCelsiusToFahrenheit() {
        XCTAssertEqual(String(format: "%.2f", TemperatureUtils.celsiusToFahrenheit(with: -273.15)), "-459.67")
        XCTAssertEqual(String(format: "%.2f", TemperatureUtils.celsiusToFahrenheit(with: 0)), "32.00")
        XCTAssertEqual(String(format: "%.2f", TemperatureUtils.celsiusToFahrenheit(with: 1000)), "1832.00")
    }
    
    func testFahrenheitToCelsius() {
        XCTAssertEqual(String(format: "%.2f", TemperatureUtils.fahrenheitToCelsius(with: -459.67)), "-273.15")
        XCTAssertEqual(String(format: "%.2f", TemperatureUtils.fahrenheitToCelsius(with: 32.0)), "0.00")
        XCTAssertEqual(String(format: "%.2f", TemperatureUtils.fahrenheitToCelsius(with: 1832.0)), "1000.00")
    }
    
}
