//
//  Created by Pongprera Punnyarphattreephop on 8/8/18.
//  Copyright © 2018 Pongprera. All rights reserved.
//

import UIKit
import Alamofire

class AFSessionManager {
    static let manager: SessionManager = {
        let serverTrustPolicies: [String: ServerTrustPolicy] = [
            NETWORK.getHostUrl(): .disableEvaluation
        ]
        
        let cookies = HTTPCookieStorage.shared
        let configuration = URLSessionConfiguration.default
        configuration.httpAdditionalHeaders = Alamofire.SessionManager.defaultHTTPHeaders
        configuration.httpCookieStorage = cookies
        
        return Alamofire.SessionManager(configuration: configuration,
                                        serverTrustPolicyManager: ServerTrustPolicyManager(policies: serverTrustPolicies))
    }()
}
