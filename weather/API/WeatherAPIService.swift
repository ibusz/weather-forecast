//
//  Created by Pongprera Punnyarphattreephop on 8/8/18.
//  Copyright © 2018 Pongprera. All rights reserved.
//

import UIKit
import RxSwift
import Alamofire
import RxAlamofire
import SwiftyJSON
import CoreLocation

class WeatherAPIService {
    enum ResourcePath: String {
        case weather = "weather"
        case forecast = "forecast"
        var path: String {
            return NETWORK.URL + rawValue
        }
    }
    
    enum APIError: Error {
        case cannotParse
    }
    
    func getWeatherForecast(cityId: String)-> Observable<[Weather]> {
        let url = "\(ResourcePath.forecast.path)?id=\(cityId)&appid=\(CONFIG.API_KEY)"
        return AFSessionManager.manager.rx.json(.get, url, encoding: JSONEncoding.default)
            .map(JSON.init)
            .flatMap { json -> Observable<[Weather]> in
                guard let weathers = Weathers(json: json["list"]) else {
                    return Observable.error(APIError.cannotParse)
                }
                return Observable.just(weathers.weathers)
        }
    }
    
    func getWeather(cityName: String)-> Observable<Weather> {
        let url = "\(ResourcePath.weather.path)?q=\(cityName)&appid=\(CONFIG.API_KEY)"
        return AFSessionManager.manager.rx.json(.get, url, encoding: JSONEncoding.default)
            .map(JSON.init)
            .flatMap { json -> Observable<Weather> in
                guard let weather = Weather(json: json) else {
                    return Observable.error(APIError.cannotParse)
                }
                return Observable.just(weather)
        }
    }
    
    func getWeather(cityId: String)-> Observable<Weather> {
        let url = "\(ResourcePath.weather.path)?id=\(cityId)&appid=\(CONFIG.API_KEY)"
        return AFSessionManager.manager.rx.json(.get, url, encoding: JSONEncoding.default)
            .map(JSON.init)
            .flatMap { json -> Observable<Weather> in
                guard let weather = Weather(json: json) else {
                    return Observable.error(APIError.cannotParse)
                }
                return Observable.just(weather)
        }
    }
    
    func getWeather(coord: CLLocationCoordinate2D)-> Observable<Weather> {
        let url = "\(ResourcePath.weather.path)?lat=\(coord.latitude)&lon=\(coord.longitude)&appid=\(CONFIG.API_KEY)"
        return AFSessionManager.manager.rx.json(.get, url, encoding: JSONEncoding.default)
            .map(JSON.init)
            .flatMap { json -> Observable<Weather> in
                guard let weather = Weather(json: json) else {
                    return Observable.error(APIError.cannotParse)
                }
                return Observable.just(weather)
        }
    }
    
    func getWeather(zip: String, countryCode: String)-> Observable<Weather> {
        let url = "\(ResourcePath.weather.path)?zip=\(zip),\(countryCode)&appid=\(CONFIG.API_KEY)"
        return AFSessionManager.manager.rx.json(.get, url, encoding: JSONEncoding.default)
            .map(JSON.init)
            .flatMap { json -> Observable<Weather> in
                guard let weather = Weather(json: json) else {
                    return Observable.error(APIError.cannotParse)
                }
                return Observable.just(weather)
        }
    }
}
