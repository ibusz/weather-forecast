//
//  Created by Pongprera Punnyarphattreephop on 8/8/18.
//  Copyright © 2018 Pongprera. All rights reserved.
//

class TemperatureUtils {
    static func kelvinToCelsius(with kelvin: Double)-> Double {
        return kelvin - 273.15
    }
    
    static func kelvinToFahrenheit(with kelvin: Double)-> Double {
        return (self.kelvinToCelsius(with: kelvin) * 9.0 / 5.0) + 32.0
    }
    
    static func celsiusToFahrenheit(with celsius: Double)-> Double {
        return (celsius * 9.0 / 5.0) + 32.0
    }
    
    static func fahrenheitToCelsius(with fahrenheit: Double)-> Double {
        return (fahrenheit - 32.0) * 5.0 / 9.0
    }
}
