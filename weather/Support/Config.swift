//
//  Created by Pongprera Punnyarphattreephop on 8/8/18.
//  Copyright © 2018 Pongprera. All rights reserved.
//

import UIKit

import Foundation

struct NETWORK {
    static let URL_MAIN = "\(NETWORK.getHostUrl())"
    static let IMAGE_URL = "https://\(NETWORK.getHostUrl())/img/w/"
    static let URL = "https://api.\(NETWORK.getHostUrl())/data/2.5/"
    
    static func getHostUrl() -> String
    {
        return "openweathermap.org"
    }
}

struct CONFIG {
    static let API_KEY = "2c2c346e51e6dc89b4ab06fc9632b28f"
    static let DEADLINE = 0.2
}

enum TabbarIndex {
    case home
    case timetable
    case notification
    case profile
}
