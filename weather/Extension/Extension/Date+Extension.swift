//
//  Created by Pongprera Punnyarphattreephop on 8/8/18.
//  Copyright © 2018 Pongprera. All rights reserved.
//

import UIKit

extension Date {
    init?(millisec: Double) {
        let timeInterval = TimeInterval(millisec)
        self = Date(timeIntervalSince1970: timeInterval)
    }
    
    func stringFromDateWithFormat(style: DateFormatter.Style)-> String {
        let formatter = DateFormatter()
        formatter.locale = Locale(identifier: "en_US")
        formatter.dateStyle = style
        formatter.timeStyle = style
        
        return formatter.string(from: self)
    }
    
    func stringDateWithFormat(format: String)-> String {
        let formatter = DateFormatter()
        formatter.locale = Locale(identifier: "en_US")
        formatter.dateFormat = format
        
        return formatter.string(from: self)
    }
}
