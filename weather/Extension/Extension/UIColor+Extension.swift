//
//  Created by Pongprera Punnyarphattreephop on 8/8/18.
//  Copyright © 2018 Pongprera. All rights reserved.
//

import UIKit

extension UIColor {
    open class var theme_dark: UIColor {
        return UIColor(red256: 36, green256: 132, blue256: 199, alpha100: 100)
    }
    open class var theme_normal: UIColor {
        return UIColor(red256: 9, green256: 155, blue256: 219, alpha100: 100)
    }
    open class var theme_light: UIColor {
        return UIColor(red256: 117, green256: 199, blue256: 235, alpha100: 100)
    }
    
    convenience init(red256: UInt8, green256:UInt8 , blue256:UInt8 , alpha100: UInt8) {
        self.init(red: CGFloat(red256)/255.0, green: CGFloat(green256)/255.0, blue: CGFloat(blue256)/255.0, alpha: CGFloat(alpha100)/100.0)
    }
    
    convenience init(rgbValue: UInt) {
        self.init(red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
                  green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
                  blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
                  alpha: CGFloat(1.0))
    }
}
