//
//  Created by Pongprera Punnyarphattreephop on 8/8/18.
//  Copyright © 2018 Pongprera. All rights reserved.
//

import UIKit

extension UIImage {
    open class var placeholderImage: UIImage {
        return UIImage(named: "NO_IMAGE")!
    }
}
