//
//  Created by Pongprera Punnyarphattreephop on 8/8/18.
//  Copyright © 2018 Pongprera. All rights reserved.
//

import UIKit
import Kingfisher

extension UIImageView {
    func setImageId(_ imageId: String) {
        self.setImageURL("\(NETWORK.IMAGE_URL)\(imageId).png")
    }
    
    func setImageURL(_ imageURL: String) {
        let urlString = "\(imageURL)"
        
        if let url = URL(string: urlString) {
            let cookies = HTTPCookieStorage.shared
            let configuration = KingfisherManager.shared.downloader.sessionConfiguration
            configuration.httpCookieStorage = cookies
            KingfisherManager.shared.downloader.sessionConfiguration = configuration
            
            self.kf.setImage(with: url,
                             placeholder: UIImage.placeholderImage,
                             options: nil,
                             progressBlock: {
                                receivedSize, totalSize in
            },
                             completionHandler: {
                                image, error, cacheType, imageURL in
//                                    print("Download Image Finished->\(cacheType)")
            })
        } else {
            self.image = UIImage.placeholderImage
        }
    }
}
