//
//  Created by Pongprera Punnyarphattreephop on 8/8/18.
//  Copyright © 2018 Pongprera. All rights reserved.
//

import Foundation

extension String {
    var decodeEmoji: String{
        let data = self.data(using: String.Encoding.utf8);
        let decodedStr = NSString(data: data!, encoding: String.Encoding.nonLossyASCII.rawValue)
        if let str = decodedStr{
            return str as String
        }
        return self
    }
    
    func withMask(mask: String) -> String {
        var resultString = String()
        
        var stringIndex = self.startIndex
        var maskIndex = mask.startIndex
        
        while stringIndex < self.endIndex && maskIndex < mask.endIndex {
            if (mask[maskIndex] == "#") {
                resultString.append(self[stringIndex])
                stringIndex = self.index(stringIndex, offsetBy: 1) //stringIndex.successor()
            } else {
                resultString.append(mask[maskIndex])
            }
            maskIndex = mask.index(maskIndex, offsetBy: 1) // = maskIndex.successor()
        }
        
        return resultString
    }
    
    func isValidEmail() -> Bool {
        let emailRegEx = "(?:[a-z0-9!#$%\\&'*+/=?\\^_`{|}~-]+(?:\\.[a-z0-9!#$%\\&'*+/=?\\^_`{|}"+"~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\"+"x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-"+"z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5"+"]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-"+"9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21"+"-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])"
        
        let emailTest = NSPredicate(format:"SELF MATCHES[c] %@", emailRegEx)
        return emailTest.evaluate(with: self)
    }

    
    func subscriptChar(i: Int) -> String {
        let indexStartOfText = self.index(self.startIndex, offsetBy: i)
        let indexEndOfText = self.index(self.startIndex, offsetBy: i+1)
        
        return String(self[indexStartOfText..<indexEndOfText])
    }
}
