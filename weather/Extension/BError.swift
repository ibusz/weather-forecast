//
//  Created by Pongprera Punnyarphattreephop on 8/8/18.
//  Copyright © 2018 Pongprera. All rights reserved.
//

import UIKit

struct BError {
    var responseCode: Int
    var responseMessage: String
    
    init() {
        responseCode = 0
        responseMessage = ""
    }
    
    init(responseCode: Int, responseMessage: String) {
        self.responseCode = responseCode
        self.responseMessage = responseMessage
    }
        
}
