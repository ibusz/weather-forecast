//
//  Created by Pongprera Punnyarphattreephop on 8/8/18.
//  Copyright © 2018 Pongprera. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import Whisper

class BUITableViewController: UITableViewController, NVActivityIndicatorViewable {
    //MARK: - Refresh
    var delegate: BTableViewDelegate?
    
    deinit {
        print("Deinit \(NSStringFromClass(type(of: self)))")
    }
    
    override func viewDidLoad() {
        self.setLabelLanguage()
        self.hideKeyboardWhenTappedAround()
    }
    
    func setLabelLanguage() {
        
    }
    
    func setPullToRefresh() {
        self.refreshControl?.addTarget(self, action: #selector(self.refresh), for: UIControlEvents.valueChanged)
    }
    
    @objc(refresh)
    func refresh() {
        delegate?.refresh()
    }
    
    //MARK: - Alert
    func showLoading() {
        self.hideErrorNotification()
        let size = CGSize(width: 30, height:30)
        startAnimating(size, message: "Please Wait", type: NVActivityIndicatorType(rawValue: 23)!)
    }
    
    func stopLoading() {
        stopAnimating()
    }
    
    func showError(bError: BError, check401: Bool? = true) {
        let alertError = UIAlertController(title: "Error", message: "\(bError.responseMessage)", preferredStyle: UIAlertControllerStyle.alert)
        
        alertError.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        
        DispatchQueue.main.asyncAfter(deadline: .now() + CONFIG.DEADLINE) {
            self.present(alertError, animated: true, completion: nil)
        }
    }
    
    func showErrorNotification(bError: BError, action: WhisperAction = . present) {
        if self.navigationController != nil {
            let message = Message(title: "\(bError.responseMessage)", textColor: .white, backgroundColor: .lightGray)
            Whisper.show(whisper: message, to: self.navigationController!, action: action)
        }
    }
    
    func hideErrorNotification() {
        if self.navigationController != nil {
            Whisper.hide(whisperFrom: self.navigationController!)
        }
    }
    
    func showNotification(title: String, action: WhisperAction = . present) {
        if self.navigationController != nil {
            let message = Message(title: title, textColor: .white, backgroundColor: UIColor.init(red256: 0, green256: 152, blue256: 0, alpha100: 100))
            Whisper.show(whisper: message, to: self.navigationController!, action: action)
        }
    }
    
    //MARK: - View
    func dismissView() {
        DispatchQueue.main.asyncAfter(deadline: .now() + CONFIG.DEADLINE) {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    func popView() {
        DispatchQueue.main.asyncAfter(deadline: .now() + CONFIG.DEADLINE) {
            _ = self.navigationController?.popViewController(animated: true)
        }
    }
    
    //MARK: - Keyboard
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(BUITableViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}

protocol BTableViewDelegate {
    func refresh()
}
