//
//  Created by Pongprera Punnyarphattreephop on 8/8/18.
//  Copyright © 2018 Pongprera. All rights reserved.
//

import UIKit
import SwiftyJSON
import CoreLocation

enum weatherUnit: String {
    case celsius = "celsius"
    case fahrenheit = "fahrenheit"
}

struct Weathers {
    var weathers: [Weather]
    
    init() {
        self.weathers = []
    }
    
    init?(json: JSON) {
        if let data = json.array {
            self.weathers = data.compactMap(Weather.init)
        } else {
            self.weathers = []
        }
    }
}

struct Weather {
    let coord: CLLocationCoordinate2D
    let weatherInfo: [WeatherInfo]
    let base: String
    let main: Main
    let visibility: Int
    let wind: Wind
    let clouds: Cloud
    let dt: Date
    let sys: Sys
    let id: Int
    let name: String
    let cod: Int
    
    init() {
        self.coord = CLLocationCoordinate2D()
        self.weatherInfo = []
        self.base = ""
        self.main = Main()
        self.visibility = -1
        self.wind = Wind()
        self.clouds = Cloud()
        self.dt = Date(millisec: 0.0)!
        self.sys = Sys()
        self.id = -1
        self.name = ""
        self.cod = -1
    }
    
    init?(json: JSON) {
        if let lon = json["coord"]["lon"].double, let lat = json["coord"]["lat"].double {
            self.coord = CLLocationCoordinate2D(latitude: lat, longitude: lon)
        } else {
            self.coord = CLLocationCoordinate2D()
        }
        
        if let weatherInfoes = WeatherInfoes(json: json["weather"]) {
            self.weatherInfo = weatherInfoes.weatherInfoes
        } else {
            self.weatherInfo = []
        }
        
        if let base = json["base"].string {
            self.base = base
        } else {
            self.base = ""
        }
        
        if let main = Main(json: json["main"]) {
            self.main = main
        } else {
            self.main = Main()
        }
        
        if let visibility = json["visibility"].int {
            self.visibility = visibility
        } else {
            self.visibility = -1
        }
        
        if let wind = Wind(json: json["wind"]) {
            self.wind = wind
        } else {
            self.wind = Wind()
        }
        
        if let clouds = Cloud(json: json["clouds"]) {
            self.clouds = clouds
        } else {
            self.clouds = Cloud()
        }
        
        if let dt = json["dt"].double {
            self.dt = Date(millisec: dt)!
        } else {
            self.dt = Date(millisec: 0.0)!
        }
        
        if let sys = Sys(json: json["sys"]) {
            self.sys = sys
        } else {
            self.sys = Sys()
        }
        
        if let id = json["id"].int {
            self.id = id
        } else {
            self.id = -1
        }
        
        if let name = json["name"].string {
            self.name = name
        } else {
            self.name = ""
        }
        
        if let cod = json["cod"].int {
            self.cod = cod
        } else {
            self.cod = -1
        }
    }
}
