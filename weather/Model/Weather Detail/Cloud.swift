//
//  Created by Pongprera Punnyarphattreephop on 8/8/18.
//  Copyright © 2018 Pongprera. All rights reserved.
//

import UIKit
import SwiftyJSON

struct Cloud {
    let all: Int
    
    init() {
        self.all = -1
    }
    
    init?(json: JSON) {
        if let all = json["all"].int {
            self.all = all
        } else {
            self.all = -1
        }
    }
}
