//
//  Created by Pongprera Punnyarphattreephop on 8/8/18.
//  Copyright © 2018 Pongprera. All rights reserved.
//

import UIKit
import SwiftyJSON

struct Sys {
    let type: Int
    let id: Int
    let message: Double
    let country: String
    let sunrise: Date
    let sunset: Date
    
    init() {
        self.type = -1
        self.id = -1
        self.message = -1.0
        self.country = ""
        self.sunrise = Date(millisec: 0.0)!
        self.sunset = Date(millisec: 0.0)!
    }
    
    init?(json: JSON) {
        if let type = json["type"].int {
            self.type = type
        } else {
            self.type = -1
        }
        
        if let id = json["id"].int {
            self.id = id
        } else {
            self.id = -1
        }
        
        if let message = json["message"].double {
            self.message = message
        } else {
            self.message = -1.0
        }
        
        if let country = json["country"].string {
            self.country = country
        } else {
            self.country = ""
        }
        
        if let sunrise = json["sunrise"].double {
            self.sunrise = Date(millisec: sunrise)!
        } else {
            self.sunrise = Date(millisec: 0.0)!
        }
        
        if let sunset = json["sunset"].double {
            self.sunset = Date(millisec: sunset)!
        } else {
            self.sunset = Date(millisec: 0.0)!
        }
    }
}
