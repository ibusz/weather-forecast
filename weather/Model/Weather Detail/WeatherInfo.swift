//
//  Created by Pongprera Punnyarphattreephop on 8/8/18.
//  Copyright © 2018 Pongprera. All rights reserved.
//

import UIKit
import SwiftyJSON

struct WeatherInfoes {
    var weatherInfoes: [WeatherInfo]
    
    init() {
        self.weatherInfoes = []
    }
    
    init?(json: JSON) {
        if let data = json.array {
            self.weatherInfoes = data.compactMap(WeatherInfo.init)
        } else {
            self.weatherInfoes = []
        }
    }
}

struct WeatherInfo {
    let id: Int
    let main: String
    let description: String
    let icon: String
    
    init() {
        self.id = -1
        self.main = ""
        self.description = ""
        self.icon = ""
    }
    
    init?(json: JSON) {
        if let id = json["id"].int {
            self.id = id
        } else {
            self.id = -1
        }
        
        if let main = json["main"].string {
            self.main = main
        } else {
            self.main = ""
        }
        
        if let description = json["description"].string {
            self.description = description
        } else {
            self.description = ""
        }
        
        if let icon = json["icon"].string {
            self.icon = icon
        } else {
            self.icon = ""
        }
    }
}
