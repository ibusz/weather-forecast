//
//  Created by Pongprera Punnyarphattreephop on 8/8/18.
//  Copyright © 2018 Pongprera. All rights reserved.
//

import UIKit

struct Temperature {
    let celsius: Double
    let fahrenheit: Double
    
    init() {
        self.celsius = -275.0
        self.fahrenheit = -275.0
    }
    
    init(kelvin: Double) {
        self.celsius = TemperatureUtils.kelvinToCelsius(with: kelvin)
        self.fahrenheit = TemperatureUtils.kelvinToFahrenheit(with: kelvin)
    }
    
    init(celsius: Double) {
        self.celsius = celsius
        self.fahrenheit = TemperatureUtils.celsiusToFahrenheit(with: celsius)
    }
    
    init(fahrenheit: Double) {
        self.celsius = TemperatureUtils.fahrenheitToCelsius(with: fahrenheit)
        self.fahrenheit = fahrenheit
        
    }
}
