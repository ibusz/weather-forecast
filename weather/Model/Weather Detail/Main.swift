//
//  Created by Pongprera Punnyarphattreephop on 8/8/18.
//  Copyright © 2018 Pongprera. All rights reserved.
//

import UIKit
import SwiftyJSON

struct Main {
    let temp: Temperature
    let pressure: Int
    let humidity: Int
    let temp_min: Temperature
    let temp_max: Temperature
    
    init() {
        self.temp = Temperature()
        self.pressure = -1
        self.humidity = -1
        self.temp_min = Temperature()
        self.temp_max = Temperature()
    }
    
    init?(json: JSON) {
        if let temp = json["temp"].double {
            self.temp = Temperature(kelvin: temp)
        } else {
            self.temp = Temperature()
        }
        
        if let pressure = json["pressure"].int {
            self.pressure = pressure
        } else {
            self.pressure = -1
        }
        
        if let humidity = json["humidity"].int {
            self.humidity = humidity
        } else {
            self.humidity = -1
        }
        
        if let temp_min = json["temp_min"].double {
            self.temp_min = Temperature(kelvin: temp_min)
        } else {
            self.temp_min = Temperature()
        }
        
        if let temp_max = json["temp_max"].double {
            self.temp_max = Temperature(kelvin: temp_max)
        } else {
            self.temp_max = Temperature()
        }
    }
}
