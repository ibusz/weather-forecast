//
//  Created by Pongprera Punnyarphattreephop on 8/8/18.
//  Copyright © 2018 Pongprera. All rights reserved.
//

import UIKit
import SwiftyJSON

struct Wind {
    let speed: Double
    let deg: Int
    
    init() {
        self.speed = -1.0
        self.deg = -1
    }
    
    init?(json: JSON) {
        if let speed = json["speed"].double {
            self.speed = speed
        } else {
            self.speed = -1.0
        }
        
        if let deg = json["deg"].int {
            self.deg = deg
        } else {
            self.deg = -1
        }
    }
}
