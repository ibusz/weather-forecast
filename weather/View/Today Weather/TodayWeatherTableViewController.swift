//
//  Created by Pongprera Punnyarphattreephop on 8/8/18.
//  Copyright © 2018 Pongprera. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class TodayWeatherTableViewController: BUITableViewController {
    //MARK: - Model & Dependencies
    var viewModel: TodayWeatherViewModel!
    fileprivate let disposeBag = DisposeBag()
    
    //MARK: - Outlet
    @IBOutlet weak var cityNameTextField: UITextField!
    @IBOutlet weak var searchBtn: UIButton!
    
    @IBOutlet weak var cityName: UILabel!
    @IBOutlet weak var cityCode: UILabel!
    @IBOutlet weak var weatherForecastBtn: UIButton!
    
    @IBOutlet weak var weatherMain: UILabel!
    @IBOutlet weak var weatherDescription: UILabel!
    @IBOutlet weak var weatherIcon: UIImageView!
    
    @IBOutlet weak var unitsSegment: UISegmentedControl!
    @IBOutlet weak var mainTemp: UILabel!
    @IBOutlet weak var mainPressure: UILabel!
    @IBOutlet weak var mainHumidity: UILabel!
    @IBOutlet weak var mainTempMax: UILabel!
    @IBOutlet weak var mainTempMin: UILabel!
    
    @IBOutlet weak var windSpeed: UILabel!
    @IBOutlet weak var windDeg: UILabel!
    
    @IBOutlet weak var cloudAll: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //init
        self.setPullToRefresh()
        viewModel = TodayWeatherViewModel()
        
        viewModel.weatherData
            .subscribe(onNext: { [weak self] in
                if ($0.cod > 0) {
                    self?.cityName.text = "\($0.name)"
                    self?.cityCode.text = "\($0.cod)"
                    
                    if $0.weatherInfo.count > 0 {
                        let weatherInfo = $0.weatherInfo[0]
                        self?.weatherMain.text = "\(weatherInfo.main)"
                        self?.weatherDescription.text = "\(weatherInfo.description)"
                        self?.weatherIcon.setImageId(weatherInfo.icon)
                    }
                    
                    let unit = self?.viewModel.getUnit()
                    self?.mainTemp.text = String(format: "%.2f \((unit?.rawValue)!)", unit == .celsius ? $0.main.temp.celsius : $0.main.temp.fahrenheit)
                    self?.mainPressure.text = "\($0.main.pressure) hPa"
                    self?.mainHumidity.text = "\($0.main.humidity) %"
                    self?.mainTempMax.text = String(format: "%.2f \((unit?.rawValue)!)", unit == .celsius ? $0.main.temp_max.celsius : $0.main.temp_max.fahrenheit)
                    self?.mainTempMin.text = String(format: "%.2f \((unit?.rawValue)!)", unit == .celsius ? $0.main.temp_min.celsius : $0.main.temp_min.fahrenheit)
                    
                    self?.windSpeed.text = "\($0.wind.speed) mps"
                    self?.windDeg.text = "\($0.wind.deg)"
                    
                    self?.cloudAll.text = "\($0.clouds.all)"
                }
            }).disposed(by: disposeBag)
        
        viewModel.searchValid
            .bind(to: self.searchBtn.rx.isEnabled)
            .disposed(by: disposeBag)
        
        viewModel.weatherForecastValid
            .bind(to: self.weatherForecastBtn.rx.isEnabled)
            .disposed(by: disposeBag)
        
        viewModel.loading
            .subscribe(onNext: { [weak self] in
                if ($0) {
                    self?.showLoading()
                } else {
                    self?.stopLoading()
                }
            }).disposed(by: disposeBag)
        
        viewModel.error
            .subscribe(onNext: { [weak self] in
                if ($0.responseCode > 0) {
                    self?.showError(bError: $0)
                } else if ($0.responseCode < 0) {
                    self?.showErrorNotification(bError: $0)
                }
            }).disposed(by: disposeBag)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Action
    @IBAction func onChangeUnit(_ sender: Any) {
        if (self.unitsSegment.selectedSegmentIndex == 0) {
            self.viewModel.onChangeUnit(unit: .celsius)
        } else {
            self.viewModel.onChangeUnit(unit: .fahrenheit)
        }
    }
    
    @IBAction func onChangeText(_ sender: UITextField) {
        self.viewModel?.onChangeText(cityName: self.cityNameTextField.text!)
    }
    
    @IBAction func search(_ sender: Any) {
        self.viewModel.getWeather()
    }
    
    // MARK: - Seque
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "WholeDay")
        {
            let destinationViewController = segue.destination as! ForecastWeatherTableViewController
            let destinationViewModel = ForecastWeatherViewModel(cityId: self.viewModel.getCityId(), unit: self.viewModel.getUnit())
            destinationViewController.viewModel = destinationViewModel
        }
    }
}

extension TodayWeatherTableViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        if (textField == cityNameTextField) {
            cityNameTextField.resignFirstResponder()
            self.viewModel.getWeather()
        }
        
        return true
    }
}

//MARK: - TableView & Delegate
extension TodayWeatherTableViewController {
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    override func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
}
