//
//  Created by Pongprera Punnyarphattreephop on 8/8/18.
//  Copyright © 2018 Pongprera. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import Alamofire

class TodayWeatherViewModel {
    //MARK: - Dependecies
    fileprivate let disposeBag = DisposeBag()
    fileprivate let weatherService = WeatherAPIService()
    
    //MARK: - Model
    fileprivate let weatherRelay :BehaviorRelay<Weather> = BehaviorRelay(value: Weather())
    var weatherData: Observable<Weather> {
        return weatherRelay.asObservable()
    }
    
    fileprivate var cityNameText: String = ""
    fileprivate var unit: weatherUnit = .celsius
    
    var weatherForecastValid: BehaviorSubject<Bool> = BehaviorSubject(value: false)
    var searchValid: BehaviorSubject<Bool> = BehaviorSubject(value: false)
    var loading: BehaviorSubject<Bool> = BehaviorSubject(value: false)
    var error: BehaviorSubject<BError> = BehaviorSubject(value: BError())
    
    init() {
        
    }
    
    func onChangeUnit(unit: weatherUnit) {
        self.unit = unit
        self.weatherRelay.accept(self.weatherRelay.value)
    }
    
    func getUnit()-> weatherUnit {
        return self.unit
    }
    
    func onChangeText(cityName: String) {
        self.cityNameText = cityName
        
        if (cityName != "") {
            self.searchValid.onNext(true)
        } else {
            self.searchValid.onNext(false)
        }
    }
    
    func getWeather() {
        do {
            if(!(try self.loading.value())) {
                self.loading.onNext(true)
                
                weatherService.getWeather(cityName: self.cityNameText).subscribe(onNext: {
                    self.weatherRelay.accept($0)
                    
                    self.weatherForecastValid.onNext(true)
                    self.loading.onNext(false)
                }, onError: { error in
                    self.loading.onNext(false)
                    
                    let errorNS = error as NSError
                    if (errorNS.code == -1009) {
                        self.error.onNext(BError(responseCode: errorNS.code, responseMessage: "Internet Connection Error"))
                    }
                    
                    if let errorAF = (error as? AFError) {
                        self.error.onNext(BError(responseCode: (errorAF.responseCode)!, responseMessage: "Server Error"))
                    }
                }, onCompleted: {
                    print("Completed (Get Weather)")
                }).disposed(by: disposeBag)
            }
        } catch {
            print(error)
        }
    }
    
    func getCityId()-> String {
        return "\(self.weatherRelay.value.id)"
    }
}
