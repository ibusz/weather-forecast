//
//  Created by Pongprera Punnyarphattreephop on 8/8/18.
//  Copyright © 2018 Pongprera. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import Alamofire

class ForecastWeatherViewModel {
    //MARK: - Dependecies
    fileprivate let disposeBag = DisposeBag()
    fileprivate let weatherService = WeatherAPIService()
    
    //MARK: - Model
    fileprivate let weatherRelay :BehaviorRelay<[Weather]> = BehaviorRelay(value: [])
    var weatherData: Observable<[Weather]> {
        return weatherRelay.asObservable()
    }
    
    fileprivate var cityId: String = ""
    fileprivate var unit: weatherUnit
    
    var refreshing: BehaviorSubject<Bool> = BehaviorSubject(value: false)
    var searchValid: BehaviorSubject<Bool> = BehaviorSubject(value: false)
    var loading: BehaviorSubject<Bool> = BehaviorSubject(value: false)
    var error: BehaviorSubject<BError> = BehaviorSubject(value: BError())
    
    init(cityId: String, unit: weatherUnit) {
        self.cityId = cityId
        self.unit = unit
        
        self.getForecastWeather()
    }
    
    func getUnit()-> weatherUnit {
        return self.unit
    }
    
    func getForecastWeather() {
        do {
            if(!(try self.loading.value())) {
                self.loading.onNext(true)
                
                weatherService.getWeatherForecast(cityId: self.cityId).subscribe(onNext: {
                    self.weatherRelay.accept($0)
                    
                    self.refreshing.onNext(false)
                    self.loading.onNext(false)
                }, onError: { error in
                    self.refreshing.onNext(false)
                    self.loading.onNext(false)
                    
                    let errorNS = error as NSError
                    if (errorNS.code == -1009) {
                        self.error.onNext(BError(responseCode: errorNS.code, responseMessage: "Internet Connection Error"))
                    }
                    
                    if let errorAF = (error as? AFError) {
                        self.error.onNext(BError(responseCode: (errorAF.responseCode)!, responseMessage: "Server Error"))
                    }
                }, onCompleted: {
                    print("Completed (Get Forecast Weather)")
                }).disposed(by: disposeBag)
            }
        } catch {
            print(error)
        }
    }
    
    func refresh() {
        refreshing.onNext(true)
        self.getForecastWeather()
    }
}
