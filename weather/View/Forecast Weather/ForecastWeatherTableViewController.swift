//
//  Created by Pongprera Punnyarphattreephop on 8/8/18.
//  Copyright © 2018 Pongprera. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class ForecastWeatherTableViewController: BUITableViewController {
    //MARK: - Model & Dependencies
    var viewModel: ForecastWeatherViewModel!
    fileprivate let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //init
        self.tableView.dataSource = nil
        self.setPullToRefresh()

        viewModel.weatherData
            .bind(to: self.tableView.rx.items(cellIdentifier: "ForecastWeather", cellType: ForecastWeatherTableViewCell.self)) { (row, element, cell) in
                cell.setCell(with: element, unit: self.viewModel.getUnit())
            }.disposed(by: disposeBag)
        
        viewModel.refreshing
            .subscribe(onNext: { [weak self] in
                if (!$0) {
                    self?.refreshControl?.endRefreshing()
                }
            }).disposed(by: disposeBag)
        
        viewModel.loading
            .subscribe(onNext: { [weak self] in
                if ($0) {
                    self?.showLoading()
                } else {
                    self?.stopLoading()
                }
            }).disposed(by: disposeBag)
        
        viewModel.error
            .subscribe(onNext: { [weak self] in
                if ($0.responseCode > 0) {
                    self?.showError(bError: $0)
                } else if ($0.responseCode < 0) {
                    self?.showErrorNotification(bError: $0)
                }
            }).disposed(by: disposeBag)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func refresh() {
        self.viewModel?.refresh()
    }
}

//MARK: - TableView & Delegate
extension ForecastWeatherTableViewController {
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    override func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
}
