//
//  Created by Pongprera Punnyarphattreephop on 8/8/18.
//  Copyright © 2018 Pongprera. All rights reserved.
//

import UIKit

class ForecastWeatherTableViewCell: UITableViewCell {
    //MARK: - Outlet
    @IBOutlet weak var date: UILabel!
    
    @IBOutlet weak var weatherMain: UILabel!
    @IBOutlet weak var weatherDescription: UILabel!
    @IBOutlet weak var weatherIcon: UIImageView!
    
    @IBOutlet weak var mainTemp: UILabel!
    @IBOutlet weak var mainPressure: UILabel!
    @IBOutlet weak var mainHumidity: UILabel!
    @IBOutlet weak var mainTempMax: UILabel!
    @IBOutlet weak var mainTempMin: UILabel!
    
    @IBOutlet weak var windSpeed: UILabel!
    @IBOutlet weak var windDeg: UILabel!
    
    @IBOutlet weak var cloudAll: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func setCell(with weather: Weather, unit: weatherUnit) {
        self.date.text = "\(weather.dt.stringDateWithFormat(format: "dd MMM yyyy HH:mm"))"
        
        if weather.weatherInfo.count > 0 {
            let weatherInfo = weather.weatherInfo[0]
            self.weatherMain.text = "\(weatherInfo.main)"
            self.weatherDescription.text = "\(weatherInfo.description)"
            self.weatherIcon.setImageId(weatherInfo.icon)
        }
        
        self.mainTemp.text = String(format: "%.2f \(unit.rawValue)", unit == .celsius ? weather.main.temp.celsius : weather.main.temp.fahrenheit)
        self.mainPressure.text = "\(weather.main.pressure) hPa"
        self.mainHumidity.text = "\(weather.main.humidity) %"
        self.mainTempMax.text = String(format: "%.2f \(unit.rawValue)", unit == .celsius ? weather.main.temp_max.celsius : weather.main.temp_max.fahrenheit)
        self.mainTempMin.text = String(format: "%.2f \(unit.rawValue)", unit == .celsius ? weather.main.temp_min.celsius : weather.main.temp_min.fahrenheit)
        
        self.windSpeed.text = "\(weather.wind.speed) mps"
        self.windDeg.text = "\(weather.wind.deg)"
        
        self.cloudAll.text = "\(weather.clouds.all)"
    }
}
